<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

/*Route::get('/hello', function () {
    return view('hello');
});*/

Route::get('/hello', 'PageController@hello');
Route::get('/', 'PageController@index');
Route::get('/about', 'PageController@about');
Route::get('/services', 'PageController@services');

Route::resource('/posts', 'PostController');

/*Route::get('/posts/{id}', 'PostController@show');*/
/*Route::get('/posts/create', 'PostController');*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
