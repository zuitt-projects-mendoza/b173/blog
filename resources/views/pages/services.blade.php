@extends('layouts.app_old')


@section('content')

    	<h1>Services</h1>
    	<p>This is the services section</p>

    	@if(count($services) > 0)
    		@foreach($services as $service)
    			<li>{{ $service }}</li>
    		@endforeach
    	@else
    		<p>Nothing to Display</p>
    	@endif

@endsection
